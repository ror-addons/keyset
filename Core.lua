Keyset = Keyset or {};

local VERSION_SETTINGS = 1;
local MAX_HOTBAR_SLOTS = 120; -- although there are 120 slots, only the 1st 60 are the 5 action bars, rest are for pets/stance/granted abilities/etc

local isLibSlashRegistered = false;
local isLibAddonButtonRegistered = false;
local localization = Keyset.Localization.GetMapping();

local registeredMenu = nil;
local moduleCount = 0;

local function RegisterLibs()
	if (not isLibSlashRegistered) then
		if (LibSlash) then
			LibSlash.RegisterWSlashCmd("keyset", function(args) Keyset.SlashCommand(args) end);
			isLibSlashRegistered = true;
		end
	end

	if (not isLibAddonButtonRegistered) then
		if (LibAddonButton) then
			LibAddonButton.Register("fx");
			local menu = LibAddonButton.AddCascadingMenuItem("fx", "Keyset");
			LibAddonButton.AddMenuSubitem(menu, localization["Setup.Save.Save"], Keyset.Setup.Save.Show);
			LibAddonButton.AddMenuSubitem(menu, localization["Setup.Load.Load"], Keyset.Setup.Load.Show);
			registeredMenu = menu;
			isLibAddonButtonRegistered = true;
		end
	end
end

function Keyset.AddMenuModule(name, callbackFunction)
	if (registeredMenu) then
		if (moduleCount == 0) then
			LibAddonButton.AddMenuDividerSubitem(registeredMenu);
		end
		moduleCount = moduleCount + 1;
		
		LibAddonButton.AddMenuSubitem(registeredMenu, name, callbackFunction);
		
		return true;
	end
end

function Keyset.Initialize()
	RegisterEventHandler(SystemData.Events.LOADING_END, "Keyset.OnLoadingEnd");
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "Keyset.OnLoadingEnd");
	
	Keyset.LoadSettings();
	
	Keyset.Setup.Save.Initialize();
	Keyset.Setup.Load.Initialize();
end

local function UpdateSettings()
	local settings = Keyset.Settings;
	local version = settings.Version;


	if (version == 1) then
		version = 2;
	end
end

function Keyset.LoadSettings()
	if (not Keyset.Settings) then
		Keyset.Settings = {};
	else
		UpdateSettings();
	end
	
	local settings = Keyset.Settings;
	settings.Version = VERSION_SETTINGS;

	if (not settings.Profiles) then
		settings.Profiles = {};
	end
end

function Keyset.SlashCommand(args)
	local option, value = args:match(L"([a-z0-9]+)[ ]?(.*)");
	
	if (type(option) == "wstring") then
		option = option:lower();
	end

	if (option == L"load") then
		Keyset.Setup.Load.Show();
	else
		Keyset.Setup.Save.Show();
	end
end

function Keyset.OnLoadingEnd()
	RegisterLibs();
end

function Keyset.SaveProfile(name, saveAll)
	Keyset.Settings.Profiles[name] = {};
	local profile = Keyset.Settings.Profiles[name];
	
	if (saveAll) then
		for action, actionData in pairs(SystemData.Settings.Keybindings) do
			local binding = {};
			KeyUtils.GetBindingsForAction(action, binding);
			profile[action] = binding;
		end
	else
		for slot = 1, MAX_HOTBAR_SLOTS do
			local action = "ACTION_BAR_" .. slot;
			local binding = {}
			KeyUtils.GetBindingsForAction(action, binding);
			profile[action] = binding;
		end
	end
end

local function IsTableEqual(tableA, tableB)
	if (not tableA or not tableB or type(tableA) ~= type(tableB)) then
		return false;
	end
	
	for key, value in pairs(tableA) do
		if (type(value) == "table") then
			if (not IsTableEqual(value, tableB[key])) then
				return false;
			end
		else
			if (tableB[key] ~= value) then
				return false;
			end
		end
	end

	return true;
end

local function IsBindingEqual(bindingA, bindingB)
	if (#bindingA ~= #bindingB) then
		return false;
	end
	
	for index, _ in ipairs(bindingA) do
		if (not IsTableEqual(bindingA[index], bindingB[index])) then
			return false;
		end
	end
	
	return true;
end

function Keyset.LoadProfile(name)
	local profile = Keyset.Settings.Profiles[name];
	if (not profile) then return end

	local bindingsChanged = false;

	for action, binding in pairs(profile) do
		-- note, changing keybinds can create a slight (1-2s) pause, depending on how many keybindings are modified
	
		-- unbind current keybindings
		local currentBinding = {};
		KeyUtils.GetBindingsForAction(action, currentBinding);
		
		if (not IsBindingEqual(currentBinding, binding)) then -- to prevent an even greater lag, only set modified keybinds
			if (not bindingsChanged) then
				bindingsChanged = true;
			end
			
			for index, keybind in ipairs(currentBinding) do
				RemoveBinding(action, keybind.deviceId, keybind.buttons);
			end
			
			-- bind saved keybindings
			for index, keybind in ipairs(binding) do
				AddBinding(action, keybind.deviceId, keybind.buttons)
			end
		end
	end
	
	if (bindingsChanged) then
		BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED); -- updates keybinds on bars
	end
end

function Keyset.DeleteProfile(name)
	if (Keyset.Settings.Profiles[name]) then
		Keyset.Settings.Profiles[name] = nil;
	end
end

local function CompareStrings(stringA, stringB)
	if (stringB == nil) then
		return false;
	end
	
	return WStringsCompare(stringA, stringB) < 0;
end

function Keyset.GetProfiles()
	local profiles = {};
	for name, _ in pairs(Keyset.Settings.Profiles) do
		table.insert(profiles, name);
	end
	table.sort(profiles, CompareStrings);
	
	return profiles;
end