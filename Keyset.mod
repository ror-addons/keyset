<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="Keyset" version="1.0.0" date="12/13/2010" >		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Healix" email="" />		
    <Description text="Allows saving and loading of keybind profiles" />        		
    <Dependencies>		
    </Dependencies>		
    <Files>			
      <File name="Localization.lua" />			
      <File name="Localization/enUS.lua" />			 			
      <File name="Core.lua" />			 			
      <File name="Setup/Save.lua" />			
      <File name="Setup/Save.xml" />			
      <File name="Setup/Load.lua" />			
      <File name="Setup/Load.xml" />		
    </Files>		 		
    <SavedVariables>			
      <SavedVariable name="Keyset.Settings" global="true" />		   		
    </SavedVariables>		 		
    <OnInitialize>			
      <CreateWindow name="KeysetSaveWindow" show="false" />			
      <CreateWindow name="KeysetLoadWindow" show="false" />			
      <CallFunction name="Keyset.Initialize" />		
    </OnInitialize>		
    <OnUpdate />		
    <OnShutdown />		
    <WARInfo />	
  </UiMod>
</ModuleFile>