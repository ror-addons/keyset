Keyset = Keyset or {};
if (not Keyset.Localization) then
	Keyset.Localization = {};
	Keyset.Localization.Language = {};
end

local localizationWarning = false;

function Keyset.Localization.GetMapping()

	local lang = Keyset.Localization.Language[SystemData.Settings.Language.active];
	
	if (not lang) then
		if (not localizationWarning) then
			d("Your current language is not supported. English will be used instead.");
			localizationWarning = true;
		end
		lang = Keyset.Localization.Language[SystemData.Settings.Language.ENGLISH];
	end
	
	return lang;
	
end