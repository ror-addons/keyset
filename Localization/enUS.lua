Keyset = Keyset or {};
if (not Keyset.Localization) then
	Keyset.Localization = {};
	Keyset.Localization.Language = {};
end

Keyset.Localization.Language[SystemData.Settings.Language.ENGLISH] = 
{
	["Setup.Save.Title"] = L"Save Keybindings",
	["Setup.Save.Info"] = L"Saves the current keybindings for hotbars",
	["Setup.Save.All"] = L"Save all bindings",
	["Setup.Save.Save"] = L"Save",
	
	["Setup.Load.Title"] = L"Load Keybindings",
	["Setup.Load.Info"] = L"Select a profile to load",
	["Setup.Load.Load"] = L"Load",
	["Setup.Load.Delete"] = L"Delete",
};

