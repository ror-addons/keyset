Keyset = Keyset or {};
Keyset.Setup = Keyset.Setup or {};
Keyset.Setup.Load =
{
	WindowName = "KeysetLoadWindow",
};

local windowName = Keyset.Setup.Load.WindowName;
local localization = Keyset.Localization.GetMapping();
local availableProfiles = {};

local function LoadProfiles()
	ComboBoxClearMenuItems(windowName .. "ProfilesComboBox");
	local profiles = Keyset.GetProfiles();
	
	for index, name in ipairs(profiles) do
		ComboBoxAddMenuItem(windowName .. "ProfilesComboBox", towstring(name));
	end
	
	availableProfiles = profiles;
end

function Keyset.Setup.Load.Initialize()

	LabelSetText(windowName .. "TitleLabel", localization["Setup.Load.Title"]);
	LabelSetText(windowName .. "InfoLabel", localization["Setup.Load.Info"]);
	ButtonSetText(windowName .. "LoadButton", localization["Setup.Load.Load"]);
	ButtonSetText(windowName .. "DeleteButton", localization["Setup.Load.Delete"]);

end

function Keyset.Setup.Load.Show()

	if (WindowGetShowing(windowName)) then return end
	
	LoadProfiles();	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, Keyset.Setup.Load.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	Sound.Play(Sound.WINDOW_OPEN);

end

function Keyset.Setup.Load.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	WindowUtils.RemoveFromOpenList(windowName);

end

function Keyset.Setup.Load.OnCloseLUp()

	Keyset.Setup.Load.Hide();

end

function Keyset.Setup.Load.OnHidden()
	
end

function Keyset.Setup.Load.OnLoadLClick()

	local value = ComboBoxGetSelectedMenuItem(windowName .. "ProfilesComboBox");
	if (not availableProfiles[value]) then return end
	
	Keyset.LoadProfile(availableProfiles[value]);

end

function Keyset.Setup.Load.OnDeleteLClick()

	local value = ComboBoxGetSelectedMenuItem(windowName .. "ProfilesComboBox");
	if (not availableProfiles[value]) then return end
	
	Keyset.DeleteProfile(availableProfiles[value]);
	LoadProfiles();

end