Keyset = Keyset or {};
Keyset.Setup = Keyset.Setup or {};
Keyset.Setup.Save =
{
	WindowName = "KeysetSaveWindow",
};

local windowName = Keyset.Setup.Save.WindowName;
local localization = Keyset.Localization.GetMapping();

function Keyset.Setup.Save.Initialize()

	LabelSetText(windowName .. "TitleLabel", localization["Setup.Save.Title"]);
	LabelSetText(windowName .. "InfoLabel", localization["Setup.Save.Info"]);
	LabelSetText(windowName .. "SaveAllCheckboxLabel", localization["Setup.Save.All"]);
	ButtonSetText(windowName .. "SaveButton", localization["Setup.Save.Save"]);

end

function Keyset.Setup.Save.Show()

	if (WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, true);
	WindowAssignFocus(windowName .. "NameEditBox", true);
	TextEditBoxSetText(windowName .. "NameEditBox", L"");
	
	WindowUtils.AddToOpenList(windowName, Keyset.Setup.Save.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	Sound.Play(Sound.WINDOW_OPEN);

end

function Keyset.Setup.Save.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	WindowAssignFocus(windowName .. "NameEditBox", false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	WindowUtils.RemoveFromOpenList(windowName);

end

function Keyset.Setup.Save.OnCloseLUp()

	Keyset.Setup.Save.Hide();

end

function Keyset.Setup.Save.OnHidden()
	
end

function Keyset.Setup.Save.OnSaveAllLUp()
	local isChecked = ButtonGetPressedFlag(windowName .. "SaveAll" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "SaveAll" .. "Button", isChecked);
end

function Keyset.Setup.Save.OnSaveLClick()

	local name = tostring(TextEditBoxGetText(windowName .. "NameEditBox"));
	if (not name or name:len() == 0) then return end
	local saveAll = ButtonGetPressedFlag(windowName .. "SaveAll" .. "Button");
	
	Keyset.SaveProfile(name, saveAll);
	Keyset.Setup.Save.Hide();

end